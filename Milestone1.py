import json
  
with open('/afs/cern.ch/user/j/jmontejo/public/HSSIP/combinedCLs_RPV1L_GG_rpvHF.json') as json_file:
    data = json.load(json_file)
    #print("Type:", type(data))
    #print(data)

#print(data['GG_rpvHF_1700_200_br50']['mgluino'])

experiments=len(data)

mmaxforlife = {
}

for exp in data:
    #get all existing life values 
    mmaxforlife[data[exp]['loglifetime']]=-1

for exp in data:
    #working on experiment exp with traits loglifetime, mgluino and CLs
    #if CLs is low enough, check for a new maximum mass on this lifetime
    if data[exp]['CLs']<=0.05 :
        #the mass is a string for some reason, so i must convert to int before using max
        mmaxforlife[data[exp]['loglifetime']]=max(int(data[exp]['mgluino']), mmaxforlife[data[exp]['loglifetime']])
            

lifevals=len(mmaxforlife)

for life in mmaxforlife:
    #print all the maximum mass values (except for the init)
    if life!=100000:
        print(str(life) + " sees the maximum value " + str(mmaxforlife[life]) + " with CLs under 0.05")    
